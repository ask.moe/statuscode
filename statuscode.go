package statuscode

import (
	"encoding/json"
	"net/http"
)

type Message struct {
	Key   string
	Value interface{}
}

func Return200OK(w http.ResponseWriter, messages ...Message) {
	response := make(map[string]interface{})
	for _, message := range messages {
		response[message.Key] = message.Value
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func Return400BadRequest(w http.ResponseWriter, messages ...Message) {
	response := make(map[string]interface{})
	for _, message := range messages {
		response[message.Key] = message.Value
	}
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(response)
}

func Return401Unauthorized(w http.ResponseWriter, messages ...Message) {
	response := make(map[string]interface{})
	for _, message := range messages {
		response[message.Key] = message.Value
	}
	w.WriteHeader(http.StatusUnauthorized)
	json.NewEncoder(w).Encode(response)
}

func Return403Forbidden(w http.ResponseWriter, messages ...Message) {
	response := make(map[string]interface{})
	for _, message := range messages {
		response[message.Key] = message.Value
	}
	w.WriteHeader(http.StatusForbidden)
	json.NewEncoder(w).Encode(response)
}

func Return404NotFound(w http.ResponseWriter, messages ...Message) {
	response := make(map[string]interface{})
	for _, message := range messages {
		response[message.Key] = message.Value
	}
	w.WriteHeader(http.StatusNotFound)
	json.NewEncoder(w).Encode(response)
}

func Return409Conflict(w http.ResponseWriter, messages ...Message) {
	response := make(map[string]interface{})
	for _, message := range messages {
		response[message.Key] = message.Value
	}
	w.WriteHeader(http.StatusConflict)
	json.NewEncoder(w).Encode(response)
}

func Return422UnprocessableEntity(w http.ResponseWriter, messages ...Message) {
	response := make(map[string]interface{})
	for _, message := range messages {
		response[message.Key] = message.Value
	}
	w.WriteHeader(http.StatusUnprocessableEntity)
	json.NewEncoder(w).Encode(response)
}

func Return429TooManyRequests(w http.ResponseWriter, messages ...Message) {
	response := make(map[string]interface{})
	for _, message := range messages {
		response[message.Key] = message.Value
	}
	w.WriteHeader(http.StatusTooManyRequests)
	json.NewEncoder(w).Encode(response)
}
